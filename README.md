# Authors
- Clément Legrand, clement.legrand4.etu@univ-lille.fr
- Laetitia Jourdan, laetitia.jourdan@univ-lille.fr
- Marie-Eléonore Kessaci, marie-eleonoe.kessaci@univ-lille.fr
- Diego Cattaruzza, diego.cattaruzza@centralelille.fr

# Description
This project contains the following elements:
- the detailed results obtained on each instance for each experiment (file Appendices_PPSN2022.pdf)
- the code used to run the experiments, and instructions on how execute it (archive code_ppsn2022.zip)
- the best pareto fronts obtained considering all our experiments (archive bestFronts.zip)
